package com.tgl.HW2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Modle {

	String path = "";

	public Modle() {
		this.path = new Config().getFilePath();
	}

	public Map<Integer, Employee> getData() {
		BufferedReader reader = null;

		Map<Integer, Employee> outputTable = new TreeMap<>();

		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
			String line;
			int employeeID = 1;
			while ((line = reader.readLine()) != null) {
				if (checkInput(line)) {
					String[] line_splite = line.trim().split(" ");
					outputTable.put(employeeID,
							new Employee(Integer.valueOf(line_splite[0]), Integer.valueOf(line_splite[1]),
									line_splite[2], line_splite[3], Integer.valueOf(line_splite[4]), line_splite[5]));
					employeeID++;
				}
			}
		} catch (IOException e) {
			System.out.println(e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}

		return outputTable;
	}

	public void delete(Map<Integer, Employee> employeeMap, Integer ansKey) {
		if (employeeMap.containsKey(ansKey)) {
			employeeMap.remove(ansKey);
			writeFile((TreeMap<Integer, Employee>)employeeMap);
		}
	}

	public void add(TreeMap<Integer, Employee> employeeMap, String input) {
		input = input.trim();
		if (checkInput(input)) {
			String[] input_splite = input.split(" ");
			int key = employeeMap.lastKey() + 1;
			try {
				employeeMap.put(key, new Employee(Integer.valueOf(input_splite[0]), Integer.valueOf(input_splite[1]),
						input_splite[2], input_splite[3], Integer.valueOf(input_splite[4]), input_splite[5]));
				writeFile(employeeMap);
			} catch (NumberFormatException e) {
				System.out.println(e);
			} catch (NullPointerException e) {
				System.out.println(e);
			}
		}

	}

	private boolean checkInput(String input) {
		boolean isConformFormat = false;

		String[] input_array = input.split(" ");

		if (input_array.length == 6) {
			if (checkHeight(input_array[0]) && checkBodyWeight(input_array[1]) && checkEnName(input_array[2])
					&& checkChName(input_array[3]) && checkExtension(input_array[4]) && checkEmail(input_array[5])) {
				isConformFormat = true;
			}
		}

		return isConformFormat;
	}

	private boolean checkHeight(String input) {
		int height;

		try {
			height = Integer.valueOf(input);
			if (height > 0) {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}

	private boolean checkBodyWeight(String input) {
		int bodyWeight = 0;

		try {
			bodyWeight = Integer.valueOf(input);
			if (bodyWeight > 0) {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}

	private boolean checkEnName(String enName) {

		if (enName.matches("[a-zA-Z]+-[a-zA-Z]+")) {
			return true;
		}
		return false;
	}

	private boolean checkChName(String chName) {

		if (chName.matches("[\\u4E00-\\u9FA5]{2,3}")) {
			return true;
		}
		return false;
	}

	private boolean checkExtension(String input) {
		int extension = 0;

		try {
			extension = Integer.valueOf(input);
			if (extension > 0) {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}

	private boolean checkEmail(String email) {

		if (email.matches("[a-zA-Z0-9._%+]+@[a-zA-Z0-9.-]+\\.[a-z]{2,6}\\.[a-z]*")) {
			return true;
		}
		return false;
	}
	
	private void writeFile(TreeMap<Integer, Employee> employeeMap) {
		BufferedWriter bufferWriter = null;
		StringBuilder temp = new StringBuilder();
		try {
			bufferWriter = new BufferedWriter(new FileWriter(path));
			for(Entry<Integer, Employee> entry:employeeMap.entrySet()) {
				temp.append(entry.getValue().toWriteFile()+"\n");
			}
			
			bufferWriter.write(temp.toString());
			bufferWriter.flush();
		}catch (IOException e) {
			System.out.println(e);
		} finally {
			try {
				if (bufferWriter != null) {
					bufferWriter.close();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}
		
	}

}
