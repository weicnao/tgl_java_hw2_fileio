package com.tgl.HW2;

public class Router {
	public String routerGet(Employee employee,Conditions condition) {
		
		Object ans = null;
		
		switch (condition) {
			
			case HEIGHT:
				ans = employee.getHeight();
				break;
			case BODYWEIGHT:
				ans = employee.getBodyWeight();
				break;
			case ENNAME:
				ans = employee.getEnName();
				break;
			case CHNAME:
				ans = employee.getChName();
				break;
			case EXTENSION:
				ans = employee.getExtension();
				break;
			case EMAIL:
				ans = employee.getEmail();
				break;
			case BMI:
				ans = employee.getBMI();
				break;
			default:
				break;
				
		}
		
		return  ans.toString();
	}
}
