package com.tgl.HW2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class EmployeeInformationCRUDIO {

	public static void main(String[] args) {

//		System.out.println(employeeInformationMangment("Search BODYWEIGHT 0"));
		System.out.println(employeeInforMang("SEARCH BODYWEIGHT 0"));
		System.out.println(employeeInforMang("SEARCH BODYWEIGHT 50"));
		System.out.println(employeeInforMang("SEARCH EMPLOYEEID 23"));
//		System.out.println(employeeInformationMangment("SEARCH EMPLOYEEID 0"));
		System.out.println(employeeInforMang("SEARCH ENNAME James-Wu"));
		System.out.println(employeeInforMang("SEARCH EXTENSION 6119"));
		System.out.println(employeeInforMang("SEARCH EMAIL JamesLiu@transglobe.com.tw"));
		System.out.println(employeeInforMang("SEARCH BMI 50.0"));
//		System.out.println(employeeInformationMangment("SEARCH 0"));
//		System.out.println(employeeInformationMangment("SEARCHBODYWEIGHT0"));

		System.out.println(employeeInforMang("DELETE EMAIL JamesLiu@transglobe.com.tw","SEARCH EMAIL JamesLiu@transglobe.com.tw"));
		System.out.println();
//		System.out.println(employeeInformationMangment("SORT BODYWEIGHT 0"));
		System.out.println(employeeInforMang("SORT BODYWEIGHT"));
		System.out.println(employeeInforMang("SORT BMI"));
		System.out.println(employeeInforMang("SORT EMAIL"));
		System.out.println(employeeInforMang("SORT EMPLOYEEID"));

		System.out.println(employeeInforMang("SEARCH EXTENSION 6120","ADD 181 64 Edward-Liaw 廖強原 6120 edwardliaw@transglobe.com.tw","SEARCH EXTENSION 6120"));
//		System.out.println(employeeInformationMangment("ADD 64 Edward-Liaw 廖強原 6120 edwardliaw@transglobe.com.tw"));
//		System.out.println(employeeInformationMangment("ADD 181 49 64 Edward-Liaw 廖強原 6120 edwardliaw@transglobe.com.tw"));
		System.out.println();
		System.out.println(employeeInforMang("SEARCH EMPLOYEEID 23","DELETE EMPLOYEEID 23","SEARCH EMPLOYEEID 23"));
		System.out.println();
		System.out.println(employeeInforMang("DELETE EMPLOYEEID 0"));

		System.out.println(employeeInforMang("FINDMAXMIN BODYWEIGHT"));
		System.out.println(employeeInforMang("FINDMAXMIN BMI"));
		System.out.println(employeeInforMang("FINDMAXMIN EMPLOYEEID"));

	}

	private static String employeeInforMang(String... commons) {
		String ans = "";
		Map<Integer, Employee> employeeMap = new Modle().getData();

		try {
			for (String common : commons) {

				if (checkInput(common)) {
					String[] input_split = common.split(" ");
					switch (Options.valueOf(input_split[0])) {
					case SEARCH:
						ans += search(employeeMap, Conditions.valueOf(input_split[1]), input_split[2]).toString();
						break;
					case SORT:
						ans += sort(employeeMap, Conditions.valueOf(input_split[1])).toString();
						break;
					case ADD:
						ans += String.valueOf(add(employeeMap, common.replace("ADD", "")));
						break;
					case DELETE:
						ans += String.valueOf(delete(employeeMap, Conditions.valueOf(input_split[1]), input_split[2]));
						break;
					case FINDMAXMIN:
						ans += findMaxMin(employeeMap, Conditions.valueOf(input_split[1]));
						break;
					default:
						break;
					}
				} else {
					ans = "IllegalArgumentException";
				}
				ans+=" ";
			}

		} catch (NullPointerException e) {
			System.out.println(e);
		} catch (IllegalArgumentException e) {
			System.out.println("not support condition");
		}
		return ans;

	}

	private static boolean checkInput(String input) {
		boolean isCurrect = false;
		try {
			isCurrect = Options.valueOf(input.split(" ")[0]).getNumberOfElement() == input.split(" ").length;
		} catch (IllegalArgumentException e) {
			isCurrect = false;
			System.out.println("not support features");
		}
		return isCurrect;
	}

	private static List<Integer> getKey(Map<Integer, Employee> employeeMap, Conditions condition, String target) {
		List<Integer> ansKey = new ArrayList<Integer>();
		try {
			if (condition == Conditions.EMPLOYEEID) {
				if (employeeMap.containsKey(Integer.valueOf(target))) {
					ansKey.add(Integer.valueOf(target));
				}
			} else {
				for (Integer key : employeeMap.keySet()) {
					if (new Router().routerGet(employeeMap.get(key), condition).toString().equals(target)) {
						ansKey.add(key);
					}
				}
			}
		} catch (NullPointerException e) {
			System.out.println(e);
		} catch (NumberFormatException e) {
			System.out.println(e);
		}
		return ansKey;
	}

	private static List<String> search(Map<Integer, Employee> employeeMap, Conditions condition, String target) {
		List<Integer> ansKey = getKey(employeeMap, condition, target);
		List<String> ans = new ArrayList<>();
		if (!ansKey.isEmpty()) {
			for (Integer key : ansKey) {
				ans.add("ID = " + key +" "+ employeeMap.get(key));
			}
		} else {
			ans.add("not find");
		}
		return ans;
	}

	private static int delete(Map<Integer, Employee> employeeMap, Conditions condition, String target) {
		switch (condition) {
		case EMPLOYEEID:
		case ENNAME:
		case EXTENSION:
			List<Integer> ansKey = getKey(employeeMap, condition, target);
			if (!ansKey.isEmpty()) {
				Integer beDelete = getKey(employeeMap, condition, target).get(0);
				new Modle().delete(employeeMap, beDelete);
			}
			break;
		default:
			System.out.println("not unique");
		}
		return employeeMap.size();
	}

	private static int add(Map<Integer, Employee> inputMap, String input) {
		TreeMap<Integer, Employee> employeeMap = (TreeMap<Integer, Employee>) inputMap;
		new Modle().add(employeeMap, input);
		return employeeMap.size();
	}

	private static List<String> sort(Map<Integer, Employee> inputMap, Conditions condition) {
		TreeMap<String, Integer> employeeMap = new TreeMap<>();
		List<String> outputList = new ArrayList<>();
		if (condition == Conditions.EMPLOYEEID) {
			for (Integer key : inputMap.keySet()) {
				outputList.add("ID = " + key.toString() + "  " + inputMap.get(key));
			}
		} else {
			for (Integer key : inputMap.keySet()) {
				if (!employeeMap.containsKey(new Router().routerGet(inputMap.get(key), condition))) {
					employeeMap.put(new Router().routerGet(inputMap.get(key), condition), key);
				} else {
					Integer Identify = 0;
					while (employeeMap
							.containsKey(new Router().routerGet(inputMap.get(key), condition) + Identify)) {
						Identify++;
					}
					employeeMap.put(new Router().routerGet(inputMap.get(key), condition) + Identify, key);
				}
			}
			for (String key : employeeMap.keySet()) {
				outputList.add("ID = " + employeeMap.get(key).toString() + "  " + inputMap.get(employeeMap.get(key)));
			}
		}
		return outputList;
	}

	private static String findMaxMin(Map<Integer, Employee> employeeMap, Conditions condition) {
		switch (condition) {
		case HEIGHT:
		case BODYWEIGHT:
		case BMI:
			List<String> ans = sort(employeeMap, condition);
			return ans.get(ans.size() - 1) + "\n" + ans.get(0);
		default:
			return "not Integer";
		}
	}

}
