package com.tgl.HW2;

public enum Options {
	SEARCH(3), SORT(2), ADD(7), DELETE(3), FINDMAXMIN(2);
	private int numberOfElement;
	private Options (int numberOfElement) {
		this.numberOfElement = numberOfElement;
	}
	
	public int getNumberOfElement() {
		return this.numberOfElement;
	}
}
