package com.tgl.HW2;

public class Employee {
	private int height;
	private int bodyWeight;
	private String enName;
	private String chName;
	private int extension;
	private String email;
	private double BMI;

	public Employee(int height, int bodyWeight, String enName, String chName, int extension, String email) {
		super();
		this.height = height;
		this.bodyWeight = bodyWeight;
		this.enName = enName;
		this.chName = chName;
		this.extension = extension;
		this.email = email;
		this.BMI = bodyWeight / ((height * 0.01) * (height * 0.01));
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
		setBMI();
	}

	public int getBodyWeight() {
		return bodyWeight;
	}

	public void setBodyWeight(int bodyWeight) {
		this.bodyWeight = bodyWeight;
		setBMI();
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getChName() {
		return chName;
	}

	public void setName(String chName) {
		this.chName = chName;
	}

	public int getExtension() {
		return extension;
	}

	public void setExtension(int extension) {
		this.extension = extension;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getBMI() {
		return BMI;
	}

	private void setBMI() {
		this.BMI = this.bodyWeight / ((this.height * 0.01) * (this.height * 0.01));
	}

	@Override
	public String toString() {
		return "height=" + height + ", bodyWeight=" + bodyWeight + ", enName=" + enName + ", chName=" + chName
				+ ", extension=" + extension + ", email=" + email + ", BMI=" + String.format("%.2f", BMI);
	}
	
	public String toWriteFile() {
		return height+" "+bodyWeight+" "+enName+" "+chName+" "+extension+" "+email;
	}
	
}
